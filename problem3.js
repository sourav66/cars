/*==== Problem #3 ====
The marketing team wants the car models listed alphabetically on the website. 
Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
*/

let inventory = require("./index.js");
function carList()
{
    var arr = [];
    for(let i=0;i < 50;i ++)
    {
         let obj = inventory[i];
         arr[i] = obj.car_model;
    }
    const ans = arr.sort();
    return (ans);
    
}
module.exports = carList;
carList();