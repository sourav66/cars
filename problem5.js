/* === Problem #5 ====
The car lot manager needs to find out how many cars are older than the year 2000.
Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
*/

let inventory = require("./index.js");
let func = require("./problem4");
let arr = func();
var res = [], p =0;
function yearOld()
{
    for(let i=0;i < arr.length;i ++)
    {
         if(arr[i]<2000)
         {
            res[p] = arr[i];
            p++;
         }
    }
    //console.log(res);
    return (res);
    
}
module.exports = yearOld;
yearOld();